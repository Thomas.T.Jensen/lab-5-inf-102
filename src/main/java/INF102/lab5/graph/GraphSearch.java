package INF102.lab5.graph;

import java.util.HashSet;
import java.util.Set;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {
    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
        if (!graph.getNodes().contains(u) || !graph.getNodes().contains(v)) {
            throw new IllegalArgumentException("Node does not exist");
        }
        Set<V> visited = new HashSet<>();
        return dfs(u, v, visited);
    }

    private boolean dfs(V current, V target, Set<V> visited) {
        if (current.equals(target)) {
            return true;
        }
        visited.add(current);
        for (V neighbor : graph.getNeighbourhood(current)) {
            if (!visited.contains(neighbor) && dfs(neighbor, target, visited)) {
                return true;
            }
        }
        return false;
    }
}
